package com.jbr390.rectanglerestapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class RectangleController {
    @CrossOrigin
    @GetMapping("/rectangle-area")
    public double getArea(@RequestParam(required=true) float width
    , @RequestParam(required=true) float length) {
        Rectangle rectangle = new Rectangle(length, width);
        double area = rectangle.getArea();
        return area;
    }
    @GetMapping("/rectangle-perimeter")
    public double getPerimeter(@RequestParam(required=true) float width
    , @RequestParam(required=true) float length) {
        Rectangle rectangle = new Rectangle(length, width);
        double perimeter = rectangle.getPerimeter();
        return perimeter;
    }
}
