package com.jbr390.rectanglerestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RectanglerestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RectanglerestapiApplication.class, args);
	}

}
